var rule = {
    title:'爱车MV',
    host:'https://www.ichemv.com',
    homeUrl:'/mv/',
    url:'/mv/fyclass_fypage.html',
    searchUrl:'/search.php?key=**',
    searchable:2,
    quickSearch:0,
    class_parse:'.m_bor li;a&&Text;a&&href;/mv/(\\d+)_1.html',
    headers:{
        'User-Agent':'MOBILE_UA'
    },
    timeout:5000,
    play_parse:true,
    lazy:'',
    limit:6,
    double:false,
    推荐:'*',
    一级:'.mv_list li;.mv_name&&Text;.pic img&&src;.mv_p a:eq(0)&&Text;a&&href',
    二级:'*',
    搜索:'.play_xg li;.name&&Text;*;*;*',
}
